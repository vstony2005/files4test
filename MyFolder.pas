unit MyFolder;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Generics.Collections;

type
  /// <summary>
  /// Represents the contents of a folder.
  /// Contains all subfolders and files.
  /// </summary>
  TMyFolder = class(TObject)
  private
    FName: string;
    FNameComplete: string;
    FFolders: TObjectList<TMyFolder>;
    FFiles: TStringList;
    FNbFolders: Integer;
    FNbFiles: Integer;

    procedure SetName(const AName: string);
    function GetSubFolder(const ASub: string): TMyFolder;
  public
    /// <summary>
    /// Initialize instance of <see cref="TMyFolder">
    /// </summary>
    /// <param name="AName">Folder name to use</param>
    constructor Create(const AName: string);
    destructor Destroy; override;

    /// <summary>
    ///   Adds a file to the current folder or to a sub-folder. All necessary
    ///   subfolders will be created automatically.
    /// </summary>
    ///  <param name="AFile">Name full path name of the file to be added to the current folder</param>
    procedure AddFile(const AFile: string);
    /// <summary>
    ///   Add subfolder(s)
    /// </summary>
    ///  <param name="ASub">name complete of subfolder(s)</param>
    procedure AddSubFolder(const ASub: string);

    function GetAllFolders: TStringList;

    /// <summary>
    ///   Name of the current folder not the full path
    /// </summary>
    property Name: string read FName;
    /// <summary>
    ///   Name of complete path
    /// </summary>
    property NameComplete: string read FNameComplete;
  end;

implementation

uses
  System.IOUtils, System.Generics.Defaults;

{ TMyFolder }

constructor TMyFolder.Create(const AName: string);
begin
  SetName(AName);

  FFolders := TObjectList<TMyFolder>.Create;
  FFiles := TStringList.Create;

  FNbFolders := 0;
  FNbFiles := 0;
end;

destructor TMyFolder.Destroy;
begin
  FFolders.Free;
  FFiles.Free;

  inherited;
end;

procedure TMyFolder.SetName(const AName: string);
begin
  FNameComplete := TPath.GetDirectoryName(IncludeTrailingPathDelimiter(AName));
  FName := TPath.GetFileName(FNameComplete);
  FNameComplete := IncludeTrailingPathDelimiter(FNameComplete);
end;

function TMyFolder.GetAllFolders: TStringList;
var
  lst: TStringList;
begin
  lst := TStringList.Create;

  for var folder in FFolders do
    lst.Add(folder.Name);

  Result := lst;
end;

function TMyFolder.GetSubFolder(const ASub: string): TMyFolder;
var
  folderName: string;
  dirs: TArray<string>;
  dir, dirName: string;
  sub: TMyFolder;
  current: TMyFolder;
begin
  folderName := TPath.GetDirectoryName(IncludeTrailingPathDelimiter(ASub));

  if (not folderName.StartsWith(FNameComplete)) then
    Result := nil
  else if (folderName = FNameComplete) then
    Result := Self
  else
  begin
    folderName := folderName.Substring(FNameComplete.Length);
    dirs := folderName.Split(TPath.DirectorySeparatorChar);
    dirName := FNameComplete;
    current := Self;
    sub := nil;

    for dir in dirs do
    begin
      dirName := dirName + dir;
      sub := nil;

      for var f in current.FFolders do
      begin
        if (f.Name = dir) then
        begin
          sub := f;
          Break;
        end;
      end;

      if (not Assigned(sub)) then
      begin
        sub := TMyFolder.Create(dirName);
        current.FFolders.Add(sub);
        current.FFolders.Sort(TComparer<TMyFolder>.Construct(
          function (const l, r: TMyFolder): Integer
          begin
            Result := CompareStr(l.Name, r.Name);
          end
        ));
        current.FNbFolders := current.FNbFolders + 1;
      end;

      current := sub;
    end;

    Result := sub;
  end;
end;


procedure TMyFolder.AddSubFolder(const ASub: string);
begin
  GetSubFolder(ASub);
end;

procedure TMyFolder.AddFile(const AFile: string);
var
  dir: TMyFolder;
begin
  if (not AFile.StartsWith(FNameComplete)) then
    Exit;
  dir := GetSubFolder(TPath.GetDirectoryName(AFile));

  if (Assigned(dir)) then
  begin
    dir.FFiles.Add(TPath.GetFileName(AFile));
    dir.FFiles.Sort;
    dir.FNbFiles := dir.FNbFiles + 1;
  end;
end;

end.
