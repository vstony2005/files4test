unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Memo.Types, FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo,
  FMX.StdCtrls, FMX.Edit, FMX.Layouts, FMX.ListBox, FMX.Objects, FMX.EditBox,
  FMX.SpinBox;

type
  TForm1 = class(TForm)
    TabControl1: TTabControl;
    tabCreate: TTabItem;
    tabResult: TTabItem;
    mmoResult: TMemo;
    Layout1: TLayout;
    Label1: TLabel;
    edtFolder: TEdit;
    btnDestination: TButton;
    Label2: TLabel;
    edtSubfolder: TEdit;
    grpDirs: TGroupBox;
    grpFiles: TGroupBox;
    Splitter1: TSplitter;
    Layout3: TLayout;
    Label3: TLabel;
    spinFileMin: TSpinBox;
    Layout4: TLayout;
    Label4: TLabel;
    spinFileMax: TSpinBox;
    Line1: TLine;
    Label5: TLabel;
    Layout5: TLayout;
    btnAddExt: TButton;
    btmRemoveExt: TButton;
    ListBox1: TListBox;
    GridPanelLayout1: TGridPanelLayout;
    rbFileRandom: TRadioButton;
    rbFileFixed: TRadioButton;
    GridPanelLayout2: TGridPanelLayout;
    rbDirRandom: TRadioButton;
    rbDirFixed: TRadioButton;
    Layout2: TLayout;
    Label6: TLabel;
    spinDirMin: TSpinBox;
    Layout6: TLayout;
    Label7: TLabel;
    spinDirMax: TSpinBox;
    Line2: TLine;
    Label8: TLabel;
    chkCaracters: TCheckBox;
    chkAccents: TCheckBox;
    Layout7: TLayout;
    Label9: TLabel;
    Line3: TLine;
    GridPanelLayout3: TGridPanelLayout;
    chkSubFolders: TCheckBox;
    chkClearFolder: TCheckBox;
    Label10: TLabel;
    spinNbSubfolders: TSpinBox;
    Line4: TLine;
    screenWait: TRectangle;
    AniIndicator1: TAniIndicator;
    procedure btmRemoveExtClick(Sender: TObject);
    procedure btnAddExtClick(Sender: TObject);
    procedure btnDestinationClick(Sender: TObject);
    procedure chkSubFoldersChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rbDirChange(Sender: TObject);
    procedure rbFileChange(Sender: TObject);
  private
    procedure AddExtension(const ext: string);
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  FMX.DialogService, System.IOUtils, MyFolder;

procedure TForm1.FormCreate(Sender: TObject);
begin
  screenWait.Visible := False;
  TabControl1.ActiveTab := tabCreate;

  edtFolder.Text :=  TPath.GetDocumentsPath;
  edtSubfolder.Text := 'Tests';

  AddExtension('.txt');
  AddExtension('.zip');
  AddExtension('.mp3');
  AddExtension('.bmp');
  AddExtension('.jpeg');
  AddExtension('.png');
  AddExtension('.7z');
  AddExtension('.pas');
  AddExtension('.flac');
  AddExtension('.mp4');

  ListBox1.ItemIndex := -1;

  ListBox1.Sorted := True;

  spinFileMin.Value := 8;
  spinFileMax.Value := 20;
  spinDirMin.Value := 8;
  spinDirMax.Value := 20;

  rbFileFixed.IsChecked := True;
  rbDirFixed.IsChecked := True;

  spinNbSubfolders.Value := 5;

  chkCaracters.IsChecked := True;
  chkAccents.IsChecked := True;
  chkSubFolders.IsChecked := True;

  (*
  var f := TMyFolder.Create('c:\fuck\you');
  try
    ShowMessage(f.Name + '-' + f.NameComplete);
    f.AddSubFolder('c:\fuck\you\10');
    f.AddSubFolder('c:\fuck\you\80');
    f.AddSubFolder('c:\fuck\you\60');
    f.AddSubFolder('c:\fuck\you\40');
    f.AddSubFolder('c:\fuck\you\30\10');
    f.AddSubFolder('c:\fuck\you\70');
    f.AddSubFolder('c:\fuck\you\70\10');
    f.AddSubFolder('c:\fuck\you\20');
    f.AddSubFolder('c:\fuck\you\30');
    f.AddSubFolder('c:\fuck\you\30\20');
    f.AddSubFolder('c:\fuck\you\50');
    f.AddSubFolder('c:\fuck\you\70\20');
    f.AddSubFolder('c:\fuck\you\70\10');

    var l := f.GetAllFolders;
    for var s in l do
      ShowMessage(s);
  finally
    f.Free;
  end;
  *)
end;

procedure TForm1.btnAddExtClick(Sender: TObject);
// add extension in ListBox
begin
  screenWait.Visible := True;
  screenWait.BringToFront;

  TDialogService.InputQuery('Add Extension',
    ['Extension'],
    [''],
    procedure(const AResult: TModalResult; const AValues: array of string)
    var
      str: string;
      i: Integer;
    begin
      try
        if (AResult = mrOk) then
        begin
          str := AValues[0].Trim;
          if (str <> '') then
          begin
            if (str.Chars[0] <> '.') then
              str := Format('.%s', [str]);

            if (ListBox1.Items.IndexOf(str) < 0) then
              AddExtension(str);
          end;
        end;
      finally
        screenWait.Visible := False;
      end;
    end);
end;

procedure TForm1.AddExtension(const ext: string);
var
  i: Integer;
begin
  i := ListBox1.Items.Add(ext);
  ListBox1.ListItems[i].IsChecked := True;
end;

procedure TForm1.btmRemoveExtClick(Sender: TObject);
// remove extension in ListBox
begin
  if (ListBox1.ItemIndex >= 0) then
    ListBox1.Items.Delete(ListBox1.ItemIndex);
end;

procedure TForm1.btnDestinationClick(Sender: TObject);
var
  dir: string;
begin
  if SelectDirectory('Select directorie', edtFolder.Text, dir) then
    edtFolder.Text := dir;
end;

procedure TForm1.rbFileChange(Sender: TObject);
begin
  spinFileMin.Enabled := rbFileRandom.IsChecked;
  spinFileMax.Enabled := spinFileMin.Enabled;
end;

procedure TForm1.rbDirChange(Sender: TObject);
begin
  spinDirMin.Enabled := rbDirRandom.IsChecked;
  spinDirMax.Enabled := spinDirMin.Enabled;
end;

procedure TForm1.chkSubFoldersChange(Sender: TObject);
begin
  spinNbSubfolders.Enabled := chkSubFolders.IsChecked;
end;

initialization
  TDialogService.PreferredMode := TDialogService.tpReferredMode.ASync;

end.
