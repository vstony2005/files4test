# File4Test

Delphi 11 - FMX

## Code for create empty file

```pascal
  st := TFileStream.Create('stream_file.txt', fmCreate);
  try
  finally
    st.Free;
  end;
```

## Application

![application](imgs/app.png)
